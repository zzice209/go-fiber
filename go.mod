module github.com/zzice209/go-fiber

go 1.15

require (
	github.com/gofiber/fiber v1.14.6 // indirect
	github.com/gofiber/fiber/v2 v2.2.0
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v1.14.5 // indirect
	go.mongodb.org/mongo-driver v1.4.3 // indirect
	gorm.io/driver/sqlite v1.1.3 // indirect
	gorm.io/gorm v1.20.6
)
