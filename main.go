package main

import (
	"context"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/zzice209/go-fiber/book"
	"github.com/zzice209/go-fiber/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"

	//"gorm.io/driver/sqlite"
	//_ "gorm.io/driver/sqlite"
	//"gorm.io/gorm"
)

type Product struct {
	id uint
	Code string
	Price int16
}

func helloWorld(c *fiber.Ctx) error {
	return c.SendString("Hello World")
}

func setupRoutes(app *fiber.App) {
	app.Get("/api/v1/books", book.GetBooks)
	app.Get("/api/v1/book/:id", book.GetBook)
	app.Post("/api/v1/book", book.NewBook)
	app.Delete("/api/v1/book/:id", book.DeleteBook)
}

func initDatabase() {
	var err error
	database.DBConn, err = gorm.Open("sqlite3", "books.db")
	if err != nil {
		panic("Failed to connect database")
	}
	database.DBConn.AutoMigrate(&book.Book{})
}

func connectDb() {
	//mongodb+srv://bookstore:<password>@cluster0.hhjxq.mongodb.net/<dbname>?retryWrites=true&w=majority

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(
		"mongodb+srv://admin123:admin123@clustername.smqky.mongodb.net/bookstore?retryWrites=true&w=majority",
	))
	if err != nil { log.Fatal(err) }

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	databases, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(databases)
}


func main() {
	connectDb()
	app := fiber.New()
	initDatabase()
	setupRoutes(app);
	app.Listen(":3000");
}